# Cuando la expectativa opaca


Comencemos preguntandonos esto:
- ¿Qué es el servicio para vos mi?
- ¿Cómo salió todo la vez anterior que serviste en el congreso?
- ¿Y que te gustó de hacer eso? 
- ¿Y que no?

El mensaje de hoy no va enfocado al servicio, a lo que solemos compartir antes de cada congreso. Sinó a cuidar de qué nuestras expectativas no estén opacadando otras cosas.

> ¿Cómo es esto Marcos?. ¿Porque hablar de reducir las expectativas cuando la próxima semana tenemos **el** congreso?.

Dejame contarte. Hubo un domingo en la reunión de la mañana  donde ofrende los $100 pesos que tenía para vivir toda la semana. **Todo**. Esa mañana, mi  emocionalismo de ese momento me hizo ofrendar y no el deseo de mi corazón. En esa cirscunstancia todo lo que sentía dentro fue el empujón para ofrendar y no una previa meditación con Dios.

Y nos pasa mucho más seguido de lo que pensamos. A veces con los **si** que decimos por que estamos con todas las pilas, emocionados por una cuestión. Vamos y nos comprometemos con algo, tomamos responsabilidades sin antes haberlas analizado.  ¿Puedo tomar esa responsabilidad?, ¿Cuento con tiempo?.
Un claro ejemplo de esto también son los domingos cuando nos muestran los videos de las misiones, la gente tocada por la lástima de ver a la gente en pobreza, dolor o los medios en los que viven, van y se anotan para servir en Misiones sin antes haber orado y buscar la guia de Dios para esto.

***

Y de esto mismo hablo, de las expectativas que tenemos ahora para el congreso.
Es genial que podamos estar con todas las energias. Sabiendo que Dios nos va a hablar en esos 4 días. Nos preparamos todo un año! 

No, quedate tranquilo que en lo que considero que Dios me hablo, no es mala onda jajaja.

La idea es prevenir dolores futuros. Medicina preventiva como dice nuestro pastor Sergio.

> El caballo se alista para el día de la batalla;
Mas Jehová es el que da la victoria. Proverbios 21:31

Prevenir que por ejemplo?
- Algún compromiso **mas** que no debamos tomar (quizás por ahora).
- Un servicio limitado por alguna cuestión del pasado, una herida o diferencia que quedó y que todo el ambiente de ebullición en nuestras emoción y expectativas previos al congreso lo está opacando.

Y no, para nada estoy intentando de bajarle las ganas a lo que vamos a hacer. No para nada. Sinó que una retro a veces es más necesaria antes de dar un paso más.

***

Entonces mí llamado para hoy es que podamos hacer una pausa, orar y que Dios nos traiga revelación de cuál tiene que ser nuestro próximo paso. Que el Espíritu Santo haga lo que las palabras no pueden hacer.

Si ya hiciste esto, genial entonces! Te pido que ores por los que vamos a estar haciendo esto.

**En tus expectativas algo Dios va a desatar! Hagamos que el sacrificio en la cruz haya valido la pena.**

Oremos con gana para que Dios nos visite estos días que nos estamos preparamos para ir.

***

Mensaje compartido el día 12/04/2019 por Marcos Stupnicki